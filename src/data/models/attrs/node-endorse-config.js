export default class NodeEndorseConfig {
  constructor(options) {
    /**
     * 允许加签
     */
    this.enable = options.enable || '0';

    /**
     * 无限加签
     */
    this.unlimited = options.unlimited || '0';

    /**
     * 不可回签
     */
    this.notCountersigned = options.notCountersigned || '0';

    /**
     * 强制回签
     */
    this.mandatoryCountersignature = options.mandatoryCountersignature || '0';

    /**
     * 配置列表数据
     */
    this.circulationRules = options.circulationRule || [];
  }
}
