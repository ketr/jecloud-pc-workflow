export default class NodeApprovaNotice {
  constructor(options) {
    /**
     * 流程发起人
     */
    this.processInitiator = options.processInitiator || '0';

    /**
     * 已审批人员
     */
    this.thisNodeApproved = options.thisNodeApproved || '0';

    /**
     * 本节点已审批直属领导
     */
    this.thisNodeApprovalDirectlyUnderLeader = options.thisNodeApprovalDirectlyUnderLeader || '0';

    /**
     * 本节点已审批部门领导
     */
    this.thisNodeApprovalDeptLeader = options.thisNodeApprovalDeptLeader || '0';
  }
}
