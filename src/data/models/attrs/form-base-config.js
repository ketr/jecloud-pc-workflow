export default class FormBaseConfig {
  constructor(options) {
    this.name = options.name || '';
    /**
     * 功能编码
     */
    this.funcCode = options.funcCode || '';

    /**
     * 功能名称
     */
    this.funcName = options.funcName || '';

    /**
     * 功能Id
     */
    this.funcId = options.funcId || '';

    /**
     * 功能对应的表编码
     */
    this.tableCode = options.tableCode || '';

    /**
     * 功能对应的产品Id
     */
    this.SY_PRODUCT_ID = options.SY_PRODUCT_ID || '';

    /**
     * 功能对应的产品编码
     */
    this.SY_PRODUCT_CODE = options.SY_PRODUCT_CODE || '';

    /**
     * 功能对应的产品名称
     */
    this.SY_PRODUCT_NAME = options.SY_PRODUCT_NAME || '';

    /**
     * 附属功能编码
     */
    this.attachedFuncCodes = options.attachedFuncCodes || '';

    /**
     * 附属功能名称
     */
    this.attachedFuncNames = options.attachedFuncNames || '';

    /**
     * 附属功能Id
     */
    this.attachedFuncIds = options.attachedFuncIds || '';

    /**
     *  内容模板
     */
    this.contentModule = options.contentModule || '';

    /**
     * 流程分类名称
     */
    this.processClassificationName = options.processClassificationName || '';

    /**
     * 流程分类编码
     */
    this.processClassificationCode = options.processClassificationCode || '';

    /**
     * 流程分类Id
     */
    this.processClassificationId = options.processClassificationId || '';

    /**
     * 部署环境
     */
    this.deploymentEnvironment = options.deploymentEnvironment || 'YF';
  }
}
