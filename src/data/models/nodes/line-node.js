import BaseNode from '../base-node';
import { isNotEmpty } from '@jecloud/utils';

export default class lineNode extends BaseNode {
  constructor(options) {
    super(options);
    /**
     * 基础配置
     */
    this.properties = {
      //id，不会重复的唯一id
      resourceId: isNotEmpty(options.properties) ? options.properties.resourceId : 'line',
      //名称
      name: isNotEmpty(options.properties) ? options.properties.name : '',
      //条件
      conditionExpression:
        isNotEmpty(options.properties) && isNotEmpty(options.properties.conditionExpression)
          ? options.properties.conditionExpression
          : '',
    };

    /**
     * 样式配置
     */
    this.styleConfig = {
      //虚线
      dashed:
        isNotEmpty(options.styleConfig) && isNotEmpty(options.styleConfig.dashed)
          ? options.styleConfig.dashed
          : '0',
      //颜色
      strokeColor:
        isNotEmpty(options.styleConfig) && isNotEmpty(options.styleConfig.strokeColor)
          ? options.styleConfig.strokeColor
          : '',
      //文字大小
      fontSize:
        isNotEmpty(options.styleConfig) && isNotEmpty(options.styleConfig.fontSize)
          ? options.styleConfig.fontSize
          : '12',
      //文字样式
      fontFamily:
        isNotEmpty(options.styleConfig) && isNotEmpty(options.styleConfig.fontFamily)
          ? options.styleConfig.fontFamily
          : '宋体',
      //文字颜色
      fontColor:
        isNotEmpty(options.styleConfig) && isNotEmpty(options.styleConfig.fontColor)
          ? options.styleConfig.fontColor
          : '',
    };

    this.source = options.source || [];

    this.target = options.target || [];
  }
}
