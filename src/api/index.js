/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  APT_FORMBASE_LOAD,
  API_SELECTPERSONNEL_LOADTREEDATA,
  API_WORKFLOW_DOSAVE,
  API_WORKFLOW_UPDATEANDDEPLOY,
  API_WORKFLOW_GETMESSAGETYPE,
  API_WORKFLOW_GETINFOBYID,
} from './urls';

/**
 * 获得表数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getData(params, headers) {
  return ajax({ url: APT_FORMBASE_LOAD, params: params, headers: headers }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得角色树的数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function loadTree(params) {
  return ajax({ url: API_SELECTPERSONNEL_LOADTREEDATA, params: params }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据保存接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doSave(params) {
  return ajax({ url: API_WORKFLOW_DOSAVE, params: params, headers: { pd: 'workflow' } }).then(
    (info) => {
      if (info != '') {
        return info;
      } else {
        return Promise.reject(info);
      }
    },
  );
}

/**
 * 数据保存并发布接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdateAndDeploy(params) {
  return ajax({
    url: API_WORKFLOW_UPDATEANDDEPLOY,
    params: params,
    headers: { pd: 'workflow' },
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据保存并发布接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getMessageType(params) {
  return ajax({
    url: API_WORKFLOW_GETMESSAGETYPE,
    params: params,
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查询单条数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getInfoById(params) {
  return ajax({
    url: API_WORKFLOW_GETINFOBYID,
    params: params,
    headers: { pd: 'workflow' },
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
